import React from 'react';
import {useState,useEffect} from 'react';
import {db} from './firebase'
import {collection,  getDocs,addDoc,updateDoc,doc,deleteDoc} from 'firebase/firestore'
function App() {
  const [users,setUsers]=useState([]);
  const [newName,setNewName]=useState("")
  const [newAge,setNewAge]=useState(0)
  const userCollectionRef=collection(db,`users`)

  useEffect(()=>{
    const getUsers=async()=>{
      const data=await getDocs(userCollectionRef);
      setUsers(data.docs.map((doc)=>({...doc.data(),id:doc.id})));
    }
    getUsers();
  },[])
  //cread//
  const creadUser= async()=>{
     await addDoc(userCollectionRef,{name:newName,age:Number(newAge)});
  }
  //update//
  const updateUser=async(id,age)=>{
    const userDoc=doc(db,`users`,id);
     const newField={age:age+1};
      await updateDoc(userDoc,newField);

  }
  //delete//
  const deleteUser=async(id)=>{
    const userDoc=doc(db,`users`,id);
    await deleteDoc(userDoc);
  }
  return (
    <div className='app'>
    <input placeholder='name...' onChange={(e)=>{setNewName(e.target.value);}}/>
    <input type='number' placeholder='age...' onChange={(e)=>{setNewAge(e.target.value);}}/>
    <button onClick={creadUser}>create user</button>
    
    {users.map((user)=>{
      return(
      <div>
      
      {""}
       <h1>Name:{user.name}</h1>
       <h1>Age:{user.age}</h1>
       <button onClick={()=>{updateUser(user.id,user.age);}}>update age </button>
       <button onClick={()=>{deleteUser(user.id)}}>delete user</button>

      </div>
    )
    })}
      
    </div>
  )
}

export default App
