
import { initializeApp } from "firebase/app";
import { getFirestore} from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAf9_uxdKoCZkNvj6oc2fLWLrF6yd998GQ",
  authDomain: "firestore-59c0d.firebaseapp.com",
  projectId: "firestore-59c0d",
  storageBucket: "firestore-59c0d.appspot.com",
  messagingSenderId: "383763036836",
  appId: "1:383763036836:web:79241ef6b7e607b26f8431",
  measurementId: "G-12L5F6DHN2"
};


const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);